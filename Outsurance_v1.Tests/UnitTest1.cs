﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Outsurance_v1.Models;
using Outsurance_v1.Services;

namespace Outsurance_v1.Tests
{
    [TestClass]
    public class OutsuranceTests
    {
        private OutsuranceETL outsuranceETL = new OutsuranceETL();
        private Extraction _extraction = new Extraction();

        [TestMethod]
        public void TokenizeTest()
        {
            string record = "Jimmy,Smith,102 Long Lane,29384857";
            string[] expected = new string[]
            {
                "Jimmy",
                "Smith",
                "102 Long Lane",
                "29384857"
            };
            var actual = _extraction.Tokenize(record);

            CollectionAssert.AreEqual(expected, actual, "Actual results not the same as expected tokens.");
        }

        [TestMethod]
        public void MapTokensToUserRecordTest()
        {
            string[] tokens = new string[]
            {
                "Jimmy",
                "Smith",
                "102 Long Lane",
                "29384857"
            };

            UserRecord actual = new UserRecord()
            {
                FirstName = "Jimmy",
                LastName = "Smith",
                Address = "102 Long Lane",
                PhoneNumber = "29384857"
            };

            UserRecord expected = _extraction.MapTokensToUserRecord(tokens);

            Assert.AreEqual(JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(actual), "Actual UserRecord not the same as expected result.");
        }
    }
}
