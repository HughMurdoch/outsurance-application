﻿namespace Outsurance_v1
{
    partial class OutsuranceETL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutsuranceETL));
            this.gbSortFrequency = new System.Windows.Forms.GroupBox();
            this.rbSFDescending = new System.Windows.Forms.RadioButton();
            this.rbSFAscending = new System.Windows.Forms.RadioButton();
            this.gbSortName = new System.Windows.Forms.GroupBox();
            this.rbSNDescending = new System.Windows.Forms.RadioButton();
            this.rbSNAscending = new System.Windows.Forms.RadioButton();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.cbHeaders = new System.Windows.Forms.CheckBox();
            this.tbFileLoad = new System.Windows.Forms.TextBox();
            this.tbProcess1 = new System.Windows.Forms.TextBox();
            this.tbProcess2 = new System.Windows.Forms.TextBox();
            this.gbSortAddress = new System.Windows.Forms.GroupBox();
            this.rbSADescending = new System.Windows.Forms.RadioButton();
            this.rbSAAscending = new System.Windows.Forms.RadioButton();
            this.btnGenerateName = new System.Windows.Forms.Button();
            this.btnGenerateAddress = new System.Windows.Forms.Button();
            this.btnGenerateBoth = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbSortFrequency.SuspendLayout();
            this.gbSortName.SuspendLayout();
            this.gbSortAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbSortFrequency
            // 
            this.gbSortFrequency.Controls.Add(this.rbSFDescending);
            this.gbSortFrequency.Controls.Add(this.rbSFAscending);
            this.gbSortFrequency.Location = new System.Drawing.Point(215, 12);
            this.gbSortFrequency.Name = "gbSortFrequency";
            this.gbSortFrequency.Size = new System.Drawing.Size(177, 52);
            this.gbSortFrequency.TabIndex = 0;
            this.gbSortFrequency.TabStop = false;
            this.gbSortFrequency.Text = "Sort Name Frequency:";
            // 
            // rbSFDescending
            // 
            this.rbSFDescending.AutoSize = true;
            this.rbSFDescending.Checked = true;
            this.rbSFDescending.Location = new System.Drawing.Point(89, 20);
            this.rbSFDescending.Name = "rbSFDescending";
            this.rbSFDescending.Size = new System.Drawing.Size(82, 17);
            this.rbSFDescending.TabIndex = 1;
            this.rbSFDescending.TabStop = true;
            this.rbSFDescending.Text = "Descending";
            this.rbSFDescending.UseVisualStyleBackColor = true;
            this.rbSFDescending.CheckedChanged += new System.EventHandler(this.rbSFDescending_CheckedChanged);
            // 
            // rbSFAscending
            // 
            this.rbSFAscending.AutoSize = true;
            this.rbSFAscending.Location = new System.Drawing.Point(7, 20);
            this.rbSFAscending.Name = "rbSFAscending";
            this.rbSFAscending.Size = new System.Drawing.Size(75, 17);
            this.rbSFAscending.TabIndex = 0;
            this.rbSFAscending.Text = "Ascending";
            this.rbSFAscending.UseVisualStyleBackColor = true;
            this.rbSFAscending.CheckedChanged += new System.EventHandler(this.rbSFAscending_CheckedChanged);
            // 
            // gbSortName
            // 
            this.gbSortName.Controls.Add(this.rbSNDescending);
            this.gbSortName.Controls.Add(this.rbSNAscending);
            this.gbSortName.Location = new System.Drawing.Point(401, 12);
            this.gbSortName.Name = "gbSortName";
            this.gbSortName.Size = new System.Drawing.Size(177, 52);
            this.gbSortName.TabIndex = 2;
            this.gbSortName.TabStop = false;
            this.gbSortName.Text = "Sort Name Alphabetical:";
            // 
            // rbSNDescending
            // 
            this.rbSNDescending.AutoSize = true;
            this.rbSNDescending.Location = new System.Drawing.Point(88, 21);
            this.rbSNDescending.Name = "rbSNDescending";
            this.rbSNDescending.Size = new System.Drawing.Size(82, 17);
            this.rbSNDescending.TabIndex = 1;
            this.rbSNDescending.Text = "Descending";
            this.rbSNDescending.UseVisualStyleBackColor = true;
            this.rbSNDescending.CheckedChanged += new System.EventHandler(this.rbSNDescending_CheckedChanged);
            // 
            // rbSNAscending
            // 
            this.rbSNAscending.AutoSize = true;
            this.rbSNAscending.Checked = true;
            this.rbSNAscending.Location = new System.Drawing.Point(7, 20);
            this.rbSNAscending.Name = "rbSNAscending";
            this.rbSNAscending.Size = new System.Drawing.Size(75, 17);
            this.rbSNAscending.TabIndex = 0;
            this.rbSNAscending.TabStop = true;
            this.rbSNAscending.Text = "Ascending";
            this.rbSNAscending.UseVisualStyleBackColor = true;
            this.rbSNAscending.CheckedChanged += new System.EventHandler(this.rbSNAscending_CheckedChanged);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.Location = new System.Drawing.Point(6, 28);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(75, 23);
            this.btnLoadFile.TabIndex = 3;
            this.btnLoadFile.Text = "&Load File";
            this.btnLoadFile.UseVisualStyleBackColor = true;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // cbHeaders
            // 
            this.cbHeaders.AutoSize = true;
            this.cbHeaders.Checked = true;
            this.cbHeaders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHeaders.Location = new System.Drawing.Point(87, 32);
            this.cbHeaders.Name = "cbHeaders";
            this.cbHeaders.Size = new System.Drawing.Size(126, 17);
            this.cbHeaders.TabIndex = 4;
            this.cbHeaders.Text = "File contains headers";
            this.cbHeaders.UseVisualStyleBackColor = true;
            // 
            // tbFileLoad
            // 
            this.tbFileLoad.Location = new System.Drawing.Point(40, 85);
            this.tbFileLoad.Multiline = true;
            this.tbFileLoad.Name = "tbFileLoad";
            this.tbFileLoad.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFileLoad.Size = new System.Drawing.Size(344, 372);
            this.tbFileLoad.TabIndex = 5;
            // 
            // tbProcess1
            // 
            this.tbProcess1.Location = new System.Drawing.Point(408, 85);
            this.tbProcess1.Multiline = true;
            this.tbProcess1.Name = "tbProcess1";
            this.tbProcess1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbProcess1.Size = new System.Drawing.Size(164, 310);
            this.tbProcess1.TabIndex = 8;
            // 
            // tbProcess2
            // 
            this.tbProcess2.Location = new System.Drawing.Point(600, 85);
            this.tbProcess2.Multiline = true;
            this.tbProcess2.Name = "tbProcess2";
            this.tbProcess2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbProcess2.Size = new System.Drawing.Size(164, 310);
            this.tbProcess2.TabIndex = 9;
            // 
            // gbSortAddress
            // 
            this.gbSortAddress.Controls.Add(this.rbSADescending);
            this.gbSortAddress.Controls.Add(this.rbSAAscending);
            this.gbSortAddress.Location = new System.Drawing.Point(587, 12);
            this.gbSortAddress.Name = "gbSortAddress";
            this.gbSortAddress.Size = new System.Drawing.Size(177, 52);
            this.gbSortAddress.TabIndex = 3;
            this.gbSortAddress.TabStop = false;
            this.gbSortAddress.Text = "Sort Address Alphabetical:";
            // 
            // rbSADescending
            // 
            this.rbSADescending.AutoSize = true;
            this.rbSADescending.Location = new System.Drawing.Point(88, 21);
            this.rbSADescending.Name = "rbSADescending";
            this.rbSADescending.Size = new System.Drawing.Size(82, 17);
            this.rbSADescending.TabIndex = 1;
            this.rbSADescending.Text = "Descending";
            this.rbSADescending.UseVisualStyleBackColor = true;
            this.rbSADescending.CheckedChanged += new System.EventHandler(this.rbSADescending_CheckedChanged);
            // 
            // rbSAAscending
            // 
            this.rbSAAscending.AutoSize = true;
            this.rbSAAscending.Checked = true;
            this.rbSAAscending.Location = new System.Drawing.Point(7, 20);
            this.rbSAAscending.Name = "rbSAAscending";
            this.rbSAAscending.Size = new System.Drawing.Size(75, 17);
            this.rbSAAscending.TabIndex = 0;
            this.rbSAAscending.TabStop = true;
            this.rbSAAscending.Text = "Ascending";
            this.rbSAAscending.UseVisualStyleBackColor = true;
            this.rbSAAscending.CheckedChanged += new System.EventHandler(this.rbSAAscending_CheckedChanged);
            // 
            // btnGenerateName
            // 
            this.btnGenerateName.Location = new System.Drawing.Point(408, 401);
            this.btnGenerateName.Name = "btnGenerateName";
            this.btnGenerateName.Size = new System.Drawing.Size(164, 23);
            this.btnGenerateName.TabIndex = 10;
            this.btnGenerateName.Text = "Generate Name Text File";
            this.btnGenerateName.UseVisualStyleBackColor = true;
            this.btnGenerateName.Click += new System.EventHandler(this.btnGenerateName_Click);
            // 
            // btnGenerateAddress
            // 
            this.btnGenerateAddress.Location = new System.Drawing.Point(600, 401);
            this.btnGenerateAddress.Name = "btnGenerateAddress";
            this.btnGenerateAddress.Size = new System.Drawing.Size(164, 23);
            this.btnGenerateAddress.TabIndex = 11;
            this.btnGenerateAddress.Text = "Generate Address Text File";
            this.btnGenerateAddress.UseVisualStyleBackColor = true;
            this.btnGenerateAddress.Click += new System.EventHandler(this.btnGenerateAddress_Click);
            // 
            // btnGenerateBoth
            // 
            this.btnGenerateBoth.Location = new System.Drawing.Point(408, 430);
            this.btnGenerateBoth.Name = "btnGenerateBoth";
            this.btnGenerateBoth.Size = new System.Drawing.Size(356, 23);
            this.btnGenerateBoth.TabIndex = 12;
            this.btnGenerateBoth.Text = "Generate Both Name and Adress files";
            this.btnGenerateBoth.UseVisualStyleBackColor = true;
            this.btnGenerateBoth.Click += new System.EventHandler(this.btnGenerateBoth_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(321, 484);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(443, 65);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // OutsuranceETL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnGenerateBoth);
            this.Controls.Add(this.btnGenerateAddress);
            this.Controls.Add(this.btnGenerateName);
            this.Controls.Add(this.gbSortAddress);
            this.Controls.Add(this.tbProcess2);
            this.Controls.Add(this.tbProcess1);
            this.Controls.Add(this.tbFileLoad);
            this.Controls.Add(this.cbHeaders);
            this.Controls.Add(this.btnLoadFile);
            this.Controls.Add(this.gbSortName);
            this.Controls.Add(this.gbSortFrequency);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OutsuranceETL";
            this.Text = "Outsurance ETL";
            this.gbSortFrequency.ResumeLayout(false);
            this.gbSortFrequency.PerformLayout();
            this.gbSortName.ResumeLayout(false);
            this.gbSortName.PerformLayout();
            this.gbSortAddress.ResumeLayout(false);
            this.gbSortAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSortFrequency;
        private System.Windows.Forms.RadioButton rbSFDescending;
        private System.Windows.Forms.RadioButton rbSFAscending;
        private System.Windows.Forms.GroupBox gbSortName;
        private System.Windows.Forms.RadioButton rbSNDescending;
        private System.Windows.Forms.RadioButton rbSNAscending;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.CheckBox cbHeaders;
        private System.Windows.Forms.TextBox tbFileLoad;
        private System.Windows.Forms.TextBox tbProcess1;
        private System.Windows.Forms.TextBox tbProcess2;
        private System.Windows.Forms.GroupBox gbSortAddress;
        private System.Windows.Forms.RadioButton rbSADescending;
        private System.Windows.Forms.RadioButton rbSAAscending;
        private System.Windows.Forms.Button btnGenerateName;
        private System.Windows.Forms.Button btnGenerateAddress;
        private System.Windows.Forms.Button btnGenerateBoth;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

