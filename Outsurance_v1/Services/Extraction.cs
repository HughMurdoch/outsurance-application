﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outsurance_v1.Models;

namespace Outsurance_v1.Services
{
    public class Extraction
    {
        private static readonly char[] _tokenDelimiters = new char[] {
            '{', '}', '(', ')', '[', ']', '>', '<','-', '_', '=', '+',
            '|', '\\', ':', ';', '"', ',', '.', '/', '?', '~', '!',
            '@', '#', '$', '%', '^', '&', '*' };

        readonly Logging _logger = new Logging();

        /// <summary>
        /// Splits a string into 4 tokens
        /// </summary>
        /// <param name="text">String to tokenize</param>
        /// <returns>Tokens</returns>
        public string[] Tokenize(string text)
        {
            try
            {
                string[] tokens = text.Split(_tokenDelimiters, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 4)
                {
                    // Implement logging i.e. log4net
                    var message = "Record that was passed in does not contain the correct number of fields: 4";
                    _logger.Log(message);
                    return null;
                }

                for (int i = 0; i < tokens.Length; i++)
                {
                    var token = tokens[i];
                    if (tokens[i].Length <= 1) continue;
                    if (token.StartsWith("'") && token.EndsWith("'"))
                        tokens[i] = token.Substring(1, token.Length - 2);
                    else if (token.StartsWith("'"))
                        tokens[i] = token.Substring(1);
                    else if (token.EndsWith("'"))
                        tokens[i] = token.Substring(0, token.Length - 1);
                }

                return tokens;
            }
            catch (Exception ex)
            {
                var message = $"Exception thrown on Tokenize: " + ex.Message + Environment.NewLine;
                _logger.Log(message);
                throw;
            }
        }

        public UserRecord MapTokensToUserRecord(string[] tokens)
        {
            var userRecord = new UserRecord()
            {
                FirstName = tokens[0],
                LastName = tokens[1],
                Address = tokens[2],
                PhoneNumber = tokens[3]
            };
            return userRecord;
        }
    }
}
