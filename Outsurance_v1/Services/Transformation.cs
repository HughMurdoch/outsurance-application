﻿using System;
using System.Collections.Generic;
using Outsurance_v1.Models;

namespace Outsurance_v1.Services
{
    public class Transformation
    {
        /// <summary>
        /// Generate a dictionary of names as keys and frequency as values
        /// </summary>
        /// <param name="userRecords">List of UserRecords to process</param>
        /// <returns>Dictionary of names and frequency</returns>
        public Dictionary<string, int> GenerateStringIntNameDictionary(List<UserRecord> userRecords)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            foreach (UserRecord userRecord in userRecords)
            {
                PopulateStringIntNameDictionary(dictionary, userRecord.FirstName);
                PopulateStringIntNameDictionary(dictionary, userRecord.LastName);
            }
            return dictionary;
        }

        /// <summary>
        /// Populates the dictionary with new names or if name exists increments the frequency
        /// </summary>
        /// <param name="dictionary">Dictionary to process</param>
        /// <param name="name">Name for the key</param>
        /// <returns>Updated dictionary</returns>
        private void PopulateStringIntNameDictionary(Dictionary<string, int> dictionary, string name)
        {
            if (dictionary.ContainsKey(name))
                dictionary[name]++;
            else
                dictionary.Add(name, 1);
            return;
        }

        /// <summary>
        /// Generate a dictionary of adresses as the key and the street name (for sorting) as the value
        /// </summary>
        /// <param name="userRecords"></param>
        /// <returns></returns>
        public Dictionary<string, string> GenerateStringStringAddressDictionary(List<UserRecord> userRecords)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            foreach (UserRecord userRecord in userRecords)
            {
                dictionary.Add(userRecord.Address, userRecord.Address.Substring(userRecord.Address.IndexOf(" ", StringComparison.Ordinal)));
            }
            return dictionary;
        }

    }
}
