﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Outsurance_v1.Services
{
    public class Loading
    {
        private readonly Logging _logger = new Logging();
        public List<KeyValuePair<string, string>> SortAddressDictionary(Dictionary<string, string> dictionary, bool ascending)
        {
            var sortedList = @ascending ? dictionary.OrderBy(de => de.Value).ToList() : dictionary.OrderByDescending(de => de.Value).ToList();
            return sortedList;
        }

        public List<KeyValuePair<string, int>> SortNameDictionary(Dictionary<string, int> dictionary, bool frequencyAscending, bool nameAscending)
        {
            List<KeyValuePair<string, int>> sortedList;
            if (frequencyAscending && nameAscending)
                sortedList = dictionary.OrderBy(de => de.Value).ThenBy(de => de.Key).ToList();
            else if (frequencyAscending)
                sortedList = dictionary.OrderBy(de => de.Value).ThenByDescending(de => de.Key).ToList();
            else if (nameAscending)
                sortedList = dictionary.OrderByDescending(de => de.Value).ThenBy(de => de.Key).ToList();
            else
                sortedList = dictionary.OrderByDescending(de => de.Value).ThenByDescending(de => de.Key)
                    .ToList();
            return sortedList;
        }

        public void GenerateNameTextFile(string text)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog.DefaultExt = ".txt";
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    File.WriteAllText(saveFileDialog.FileName, text);
            }
            catch (Exception ex)
            {
                _logger.Log($"Error generating name text file: {ex.Message}");
                throw;
            }
        }

        public void GenerateAddressTextFile(string text)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog.DefaultExt = ".txt";
                saveFileDialog.RestoreDirectory = true;
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    File.WriteAllText(saveFileDialog.FileName, text);
            }
            catch (Exception ex)
            {
                _logger.Log($"Error generating address text file: {ex.Message}");
                throw;
            }

        }
    }
}
