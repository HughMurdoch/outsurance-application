﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Outsurance_v1.Models;
using Outsurance_v1.Services;

namespace Outsurance_v1
{
    public partial class OutsuranceETL : Form
    {
        // List of possible delimiters, included others for flexibility
        private List<UserRecord> _userRecords = new List<UserRecord>();

        private readonly Logging _logger = new Logging();
        private readonly Extraction _extraction = new Extraction();
        private readonly Transformation _transformation = new Transformation();
        private readonly Loading _loading = new Loading();

        public OutsuranceETL()
        {
            InitializeComponent();
        }

        /// <summary>
        /// A method that takes a string and tokenizes it using the predefined delimiters
        /// </summary>
        /// <param name="text">Text to tokenize</param>
        /// <returns>Collection of tokens</returns>
        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            try
            {
                ReadCSVFile();
                if (_userRecords.Count > 0)
                {
                    ProcessRecordsOutput1();
                    ProcessRecordsOutput2();
                }
            }
            catch (Exception ex)
            {
                _logger.Log($"Extraction error: {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Display a file open dialog and the run through the records, tokenize them and load them into the List<UserRecord>
        /// </summary>
        private void ReadCSVFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbFileLoad.Text = string.Empty;
                tbProcess1.Text = string.Empty;
                tbProcess2.Text = string.Empty;
                using (var reader = new StreamReader(openFileDialog.FileName))
                {
                    var record = reader.ReadLine();
                    if (cbHeaders.Checked)
                        record = reader.ReadLine();

                    while (!reader.EndOfStream)
                    {
                        var tokens = _extraction.Tokenize(record);
                        if (tokens.Length == 4)
                        {
                            var userRecord = _extraction.MapTokensToUserRecord(tokens);
                            DisplayRecordLoaded(userRecord);
                            _userRecords.Add(userRecord);
                        }
                        record = reader.ReadLine();
                    }
                }
            }
        }

        /// <summary>
        /// Display record loaded in the file load textbox
        /// </summary>
        /// <param name="userRecord">The user record to display</param>
        private void DisplayRecordLoaded(UserRecord userRecord)
        {
            try
            {
                tbFileLoad.AppendText($"{userRecord.FirstName} - {userRecord.LastName} - {userRecord.Address} - {userRecord.PhoneNumber}" + Environment.NewLine);
            }
            catch (Exception ex)
            {
                _logger.Log("Exception thrown: " + ex.Message + Environment.NewLine);
            }
        }

        private void ProcessRecordsOutput1()
        {
            try
            {
                tbProcess1.Text = string.Empty;
                Dictionary<string, int> dictionary = _transformation.GenerateStringIntNameDictionary(_userRecords);
                if (dictionary.Count > 0)
                {
                    var sortedList = _loading.SortNameDictionary(dictionary, rbSFAscending.Checked, rbSNAscending.Checked);
                    foreach (var item in sortedList)
                    {
                        tbProcess1.AppendText($"{item.Key} [{item.Value}]" + Environment.NewLine);
                    }
                }
            }
            catch (Exception ex)
            {
                var message = "Exception thrown on first records processing: " + ex;
                _logger.Log(message);
                throw;
            }
        }

        private void ProcessRecordsOutput2()
        {
            try
            {
                tbProcess2.Text = string.Empty;
                Dictionary<string, string> dictionary = _transformation.GenerateStringStringAddressDictionary(_userRecords);

                if (dictionary.Count > 0)
                {
                    var sortedList = _loading.SortAddressDictionary(dictionary, rbSAAscending.Checked);
                    foreach (var item in sortedList)
                    {
                        tbProcess2.AppendText($"{item.Key}" + Environment.NewLine);
                    }
                }

            }
            catch (Exception ex)
            {
                var message = "Exception thrown on second records processing: " + ex;
                _logger.Log(message);
                throw;
            }
        }

        private void rbSFAscending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput1();
        }

        private void rbSFDescending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput1();
        }

        private void rbSNAscending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput1();
        }

        private void rbSNDescending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput1();
        }

        private void rbSAAscending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput2();
        }

        private void rbSADescending_CheckedChanged(object sender, EventArgs e)
        {
            if (_userRecords.Count > 0)
                ProcessRecordsOutput2();
        }

        private void btnGenerateName_Click(object sender, EventArgs e)
        {
            GenerateNameTextFile();
        }

        private void GenerateNameTextFile()
        {
            try
            {
                if (tbProcess1.Text != string.Empty)
                    _loading.GenerateNameTextFile(tbProcess1.Text);
            }
            catch (Exception ex)
            {
                _logger.Log($"Error generating name text file: {ex.Message}");
                throw;
            }
        }

        private void btnGenerateAddress_Click(object sender, EventArgs e)
        {
            GenerateAddressTextFile();
        }

        private void GenerateAddressTextFile()
        {
            try
            {
                if (tbProcess2.Text != string.Empty)
                    _loading.GenerateAddressTextFile(tbProcess2.Text);
            }
            catch (Exception ex)
            {
                _logger.Log($"Error generating address text file: {ex.Message}");
                throw;
            }

        }

        private void btnGenerateBoth_Click(object sender, EventArgs e)
        {
            GenerateNameTextFile();
            GenerateAddressTextFile();
        }
    }
}
